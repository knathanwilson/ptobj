
# import dependencies
import datetime, re
import yaml, inflection

# plain text object functions
def pto_add_to_global(self):
    if self.__pto_has_global_key__:
        global_key = getattr(self, self.__pto_global_key__)
        if global_key is not None:
            self.__pto_instances__[global_key] = self

def pto_init(self, init=True, **kwargs):
    if init:
        for each in self.__pto_fields__:
            each.init(self, kwargs)
        pto_add_to_global(self)
        for each in self.__pto_after_inits__:
            each(self)

def pto_to_data(self, data):
    data['class'] = self.__pto_name__
    for each in self.__pto_fields__:
        each.to_data(self, data)

def pto_from_data(self, data, after_loads):
    for each in self.__pto_fields__:
        each.from_data(self, data, after_loads)
    pto_add_to_global(self)
    for each in self.__pto_after_inits__:
        each(self)


# field
class Field(object):

    def __init__(self, default=None, kind=None, refr_class=None, special_from=None, special_to=None, conv=None, conv_to=None):
        self.default = default
        self.kind = kind
        self.refr_class = refr_class
        self.name = ''
        self.dict_name = ''
        self.conv = conv
        self.conv_to = conv_to
        self._special_from = special_from
        self._special_to = special_to

    def init(self, obj, kwargs):
        setattr(obj, self.name, kwargs.get(self.name, self.default))

    def to_data(self, obj, data):
        value = getattr(obj, self.name)
        if self.kind == 'refr':
            if value is not None:
                value = getattr(value, self.refr_class.__pto_global_key__)
        elif self.kind == 'refr_list':
            if value is not None:
                value = [getattr(item, self.refr_class.__pto_global_key__) for item in value]
        elif self.kind == 'single_class':
            if value is not None:
                n_value = {}
                value.__pto_to_data__(n_value)
                n_value.pop('class')
                value = n_value
        elif self.kind == 'single_class_list':
            if value is not None:
                n_value = []
                for each in value:
                    e_value = {}
                    each.__pto_to_data__(e_value)
                    e_value.pop('class')
                    n_value.append(e_value)
                value = n_value
        elif self.kind == 'special':
            if self._special_to:
                value = self._special_to(self, value)
        if self.conv_to:
            value = self.conv_to(value)
        data[self.dict_name] = value

    def from_data(self, obj, data, after_loads):
        value = data.get(self.dict_name, self.default)
        set_value = True
        if self.kind == 'refr':
            if value is not None:
                #after_loads.append(RefrAfterLoad(obj=obj, name=self.name, instances=self.refr_class.__pto_instances__, value=value))
                Lazy(obj, self.name, lambda: setattr(obj, self.name, self.refr_class.__pto_instances__.get(value))).apply()
                set_value = False
        elif self.kind == 'refr_list':
            if value is not None:
                after_loads.append(RefrListAfterLoad(obj=obj, name=self.name, instances=self.refr_class.__pto_instances__, value=value))
                value = None
        elif self.kind == 'single_class':
            if value is not None:
                n_obj = self.refr_class(init=False)
                n_obj.__pto_from_data__(value, after_loads=after_loads)
                value = n_obj
        elif self.kind == 'single_class_list':
            if value is not None:
                n_value = []
                for each in value:
                    e_obj = self.refr_class(init=False)
                    e_obj.__pto_from_data__(each, after_loads=after_loads)
                    n_value.append(e_obj)
                value = n_value
        elif self.kind == 'special':
            if self._special_from is not None:
                value = self._special_from(self, value)
        if self.conv is not None:
            value = self.conv(value)
        if set_value:
            setattr(obj, self.name, value)

    def special_from(self, func):
        self._special_from = func
        return func

    def special_to(self, func):
        self._special_to = func
        return func


# class extension
class ClassExtender(object):
    def __init__(self, kind, func):
        self.kind = kind
        self.func = func

def after_init(func):
    return ClassExtender('after_init', func)

# lazy
class Lazy(object):

    def __init__(self, obj, name, func):
        self.obj = obj
        self.name = name
        self.func = func

    def getter(self):
        self.setter(self.func())
        return getattr(self.obj, self.name)

    def setter(self, value):
        delattr(self.obj, self.name)
        setattr(self.obj, self.name, value)

    def apply(self):
        setattr(self.obj, self.name, property(fset=self.setter, fget=self.getter))

# helper classes
class RefrAfterLoad:

    def __init__(self, obj, name, instances, value):
        self.obj = obj
        self.name = name
        self.instances = instances
        self.value = value

    def __call__(self):
        setattr(self.obj, self.name, self.instances.get(self.value))

class RefrListAfterLoad:

    def __init__(self, obj, name, instances, value):
        self.obj = obj
        self.name = name
        self.instances = instances
        self.value = value

    def __call__(self):
        setattr(self.obj, self.name, [self.instances.get(name) for name in self.value])


# register a new class
obj_classes = {}

def new_class(name=None, global_key=None):
    def __new_class_wrapper__(classobj):

        # start
        nonlocal name
        if name is None:
            name = name or inflection.underscore(classobj.__name__).replace('_', '-')
        obj_classes[name] = classobj
        fields = []
        after_inits = []
        setattr(classobj, '__pto_class__', True)
        setattr(classobj, '__pto_name__', name)
        setattr(classobj, '__pto_fields__', fields)
        setattr(classobj, '__init__', pto_init)
        setattr(classobj, '__pto_from_data__', pto_from_data)
        setattr(classobj, '__pto_to_data__', pto_to_data)
        setattr(classobj, '__pto_after_inits__', after_inits)

        # go through attrs
        for attr_enum, attr_name in enumerate(dir(classobj)):
            attr = getattr(classobj, attr_name)

            # field
            if isinstance(attr, Field):
                attr.name = attr_name
                attr.dict_name = attr_name.lower().replace('_', '-')
                if attr.refr_class:
                    if isinstance(attr.refr_class, str):
                        attr.refr_class = obj_classes[attr.refr_class]
                fields.append(attr)

            # class extender
            elif isinstance(attr, ClassExtender):
                if attr.kind == 'after_init':
                    after_inits.append(attr.func)
                delattr(classobj, attr_name)

        # global key
        setattr(classobj, '__pto_has_global_key__', global_key is not None)
        if global_key is not None:
            instances = {}
            setattr(classobj, '__pto_global_key__', global_key)
            setattr(classobj, '__pto_instances__', instances)

        # finished
        return classobj
    return __new_class_wrapper__

# string
string_dumpers = {}
string_loaders = []

def string_dumper(classobj):
    def __string_dumper_wrap__(func):
        string_dumpers[classobj] = func
        return func
    return __string_dumper_wrap__

def string_loader(func):
    string_loaders.append(func)
    return func

# datetime
to_datetime_func = datetime.datetime.strptime

datetime_formats = [
    '%Y-%m-%d',
    '%Y-%m-%d %H',
    '%Y-%m-%d %H:%M',
    '%Y-%m-%d %H:%M:%S',
    '%Y-%m-%d %H:%M:%S.%f',
]

def _numstr(num, size=2):   # number string
    _n = str(num)
    while len(_n) < size:
        _n = '0' + _n
    return _n

@string_dumper(datetime.datetime)
def datetime_dumper(obj):
    return f'{obj.year}-{_numstr(obj.month)}-{_numstr(obj.day)} {_numstr(obj.hour)}:{_numstr(obj.minute)}:{_numstr(obj.second)}'

@string_loader
def datetime_loader(obj):
    for pattern in datetime_formats:
        try:
            tm = to_datetime_func(obj, pattern)
            return tm
        except:
            pass

# time delta
timedelta_capture = re.compile('((?P<weeks>\d+?)\s*wks)?\s*((?P<days>\d+?)\s*days)?\s*((?P<hours>\d+?)\s*hrs)?\s*((?P<minutes>\d+?)\s*mins)?\s*((?P<seconds>\d+?)\s*secs)?\s*((?P<milliseconds>\d+?)\s*millis)?\s*((?P<microseconds>\d+?)\s*micros)?')

@string_loader
def load_timedelta(deltastring):

    # start
    match = timedelta_capture.match(deltastring)
    pieces = match.groupdict()

    # does this match?
    if not all(piece is None for piece in pieces.values()):
        for key in pieces:
            if pieces[key] is None:
                pieces[key] = 0
            else:
                pieces[key] = int(pieces[key])
        return datetime.timedelta(**pieces)

@string_dumper(datetime.timedelta)
def dump_timedelta(obj):
    op = ''
    for full, abrv in [
        ('days', 'days'),
        ('seconds', 'secs'),
        ('microseconds', 'micros'),
    ]:
        val = getattr(obj, full)  # value
        if val:
            if op:
                op += ' '
            op += f'{val}{abrv}'
    if not op:
        op = '0micros'
    return op

# util
def get(classname, global_key, default=None):
    return obj_classes[classname].__pto_instances__.get(global_key, default)

def get_constructor(loader, node):
    return get(node.value[0].value, node.value[1].value)

yaml.add_constructor(u'!get', get_constructor)

# io
def _dump(tabs, obj, nl_dict=True):

    # null
    if obj is None:
        return 'null'

    # bool
    if obj is True:
        return 'true'
    elif obj is False:
        return 'false'

    # string
    if isinstance(obj, str):
        if ':' in obj:
            return '"' + obj.replace('"', '\\"') + '"'
        else:
            return obj

    # number
    elif isinstance(obj, (int, float)):
        return str(obj)

    # list
    elif isinstance(obj, (list, tuple)):
        op = '\n'
        for item in obj:
            if item is not None:
                op += tabs + '- ' + _dump(tabs + '  ', item, nl_dict=False) + '\n'
        return op

    # dict
    elif isinstance(obj, dict):
        op = '\n' if nl_dict else ''
        for key, value in obj.items():
            if value is not None:
                if op:
                    op += tabs
                op += _dump('', key) + ': ' + _dump(tabs + '  ', value) + '\n'
        return op

    # class
    elif hasattr(obj, '__pto_class__'):
        data = {}
        obj.__pto_to_data__(data)
        return _dump(tabs, data, nl_dict=nl_dict)

    # string-dump
    elif obj.__class__ in string_dumpers:
        return string_dumpers[obj.__class__](obj)

def dump(obj, fileobj):
    if isinstance(fileobj, str):
        fileobj = open(fileobj, 'w')
    fileobj.write(_dump('', obj))

def dumps(obj):
    return _dump('', obj)

def _load(obj, after_loads):
    if isinstance(obj, str):
        for loader in string_loaders:
            get = loader(obj)
            if get is not None:
                return get
        return obj
    elif isinstance(obj, list):
        return [_load(item, after_loads) for item in obj]
    elif isinstance(obj, dict):
        n_dict = {_load(key, after_loads):_load(value, after_loads) for key, value in obj.items()}
        if 'class' in n_dict:
            o_obj = obj_classes[n_dict['class']](init=False)
            o_obj.__pto_from_data__(n_dict, after_loads=after_loads)
            return o_obj
        else:
            return n_dict
    else:
        return obj

def loads(text):
    after_loads = []
    output = _load(yaml.load(text, yaml.Loader), after_loads=after_loads)
    for each in after_loads:
        each()
    return output

def load(fileobj):
    if isinstance(fileobj, str):
        fileobj = open(fileobj, 'r')
    return loads(fileobj)