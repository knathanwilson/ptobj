
# import dependencies
import yaml
import datetime, re, os, types

# util

# error
class NoSuchClass(BaseException):
    def __init__(self, class_name):
        self.class_name = class_name

class NotCallable(BaseException): pass

class KindNotSupported(BaseException):
    def __init__(self, classobj):
        self.classobj = classobj

class NotDelta(BaseException):
    pass

class NoGlobalKey(BaseException):
    def __init__(self, classobj):
        pass

# globals
loader = yaml.Loader
dumper = yaml.Dumper

timetype = datetime.datetime
to_time_func = datetime.datetime.strptime
deltatype = datetime.timedelta
to_delta_func = None
pd = None

objecttype = object
strtype = str
inttype = int
floattype = float
booltype = bool
listtype = list
dicttype = dict

# time
time_formats = [
    '%Y-%m-%d %H:%M:%S',
    '%Y-%m-%d %H:%M:%S.%f',
]
time_pieces = 'aAwdbBmyYHIpMSfzZjUWcxX'
delta_capture = re.compile('((?P<weeks>\d+?)\s*wks)?\s*((?P<days>\d+?)\s*days)?\s*((?P<hours>\d+?)\s*hrs)?\s*((?P<minutes>\d+?)\s*mins)?\s*((?P<seconds>\d+?)\s*secs)?\s*((?P<milliseconds>\d+?)\s*millis)?\s*((?P<microseconds>\d+?)\s*micros)?')

def to_delta(deltastring):

    # start
    match = delta_capture.match(deltastring)
    pieces = match.groupdict()

    # does this match?
    if not all(piece is None for piece in pieces.values()):
        for key in pieces:
            if pieces[key] is None:
                pieces[key] = 0
            else:
                pieces[key] = int(pieces[key])
        return deltatype(**pieces)

    # nope!
    else:
        raise NotDelta()

to_delta_func = to_delta

# kind
kinds = []
kind_functions = []

class kind:

    def __init__(self, check=None, default=None, after_load=None, on_save=None):

        # check
        if check in [
            booltype, inttype, floattype, strtype, objecttype,
            timetype, listtype, dicttype, deltatype,
        ]:
            self.check = lambda obj: obj is check
        else:
            self.check = check

        # default
        if hasattr(default, '__call__'):
            self.default = kind.add_func(default)
        else:
            self.default = lambda: str(default)

        # after_load
        if after_load is None:
            self.after_load = lambda: ''
        elif hasattr(after_load, '__call__'):
            self.after_load = kind.add_func(after_load)
        else:
            raise NotCallable()

        # on_save
        if on_save is None:
            self.on_save = lambda: ''
        elif hasattr(on_save, '__call__'):
            self.on_save = kind.add_func(on_save)
        else:
            raise NotCallable()

        # finish
        kinds.append(self)

    @staticmethod
    def add_func(func, args = ''):
        _state = f'kind_functions[{len(kind_functions)}]({args})'
        kind_functions.append(func)
        return lambda: _state

    @staticmethod
    def get(obj):
        for item in kinds:
            if item.check(obj):
                return item
        raise KindNotSupported(obj)

def set_kinds():
    if not kinds:
        kind(objecttype, None)
        kind(booltype, False)
        kind(inttype, 0)
        kind(floattype, 0.0)
        kind(strtype, '""')
        kind(listtype, lambda: [])
        kind(dicttype, lambda: {})
        kind(timetype, lambda: timetype.now())
        kind(deltatype, lambda: deltatype(seconds=0))
        kind(Special.check, Special.default, Special.get_after_load, Special.get_on_save)

# pandas
def set_pandas ():
    global pd, timetype, to_time_func
    global time, to_delta_func
    import pandas as pd
    timetype = pd.Timestamp
    deltatype = pd.Timedelta
    to_time_func = lambda text, pattern: pd.Timestamp(datetime.datetime.strptime(text, pattern))

# obj class
obj_classes = {}

obj_globals = {}

obj_data_funcs = '''
def _init(self, **kwargs):
{}
def _todata(self, data):
{}
def _fromdata(self, data, after_loads):
{}
'''

obj_dict_refr_set = '''
 _{0}_temp_ = {1}.get("{2}", dict_refr_none)
 if _{0}_temp_ is dict_refr_none:
     self.{0} = dict_refrs[{3}].default
 else:
     self.{0} = dict_refrs[{3}].source[_{0}_temp_]
'''[1:]

def new_class(name, global_key=None):

    def _wrap(classobj):

        # start
        set_kinds()
        obj_classes[name] = classobj
        if global_key is not None:
            obj_globals[name] = {}
        kind(check=lambda obj: obj is classobj)
        _init = CCFunc()
        _todata = CCFunc()
        _fromdata = CCFunc()


        # assign name
        setattr(classobj, 'pto_classname', name)

        # assign global key
        if global_key is not None:
            setattr(classobj, 'pto_global_key', global_key)

        # assign attributes
        for _ae, _an in enumerate(dir(classobj)):   # attr-enum attr-name after-func
            if not _an.startswith('__') and not _an.startswith('pto_'):

                # start
                _attr = getattr(classobj, _an)    # attr
                _attrdictname = _an.replace('_', '-')     # attr dict-name

                # an object
                if isinstance(_attr, Obj):
                    #_init += f' self.{_an} = kwargs.get("{_an}", {_attr.default})\n'
                    cc = StandardValue(_an, _attr.default)
                    _init.funcs.append(cc.init)
                    _todata.funcs.append(cc.to_data)
                    _fromdata.funcs.append(cc.from_data)

                # a function
                elif isinstance(_attr, types.FunctionType):
                    pass

                # is this an "other"?
                elif isinstance(_attr, Other):
                    _init += f' self.{_an} = others[{_attr.index}].value\n'

                # is this a dict reference?
                elif isinstance(_attr, DictRefr):
                    _init += obj_dict_refr_set.format(_an, 'kwargs', _attrdictname, _attr.index)
                    _fromdata += obj_dict_refr_set.format(_an, 'data', _attrdictname, _attr.index)
                    _todata += f' data["{_attrdictname}"] = dict_refrs[{_attr.index}].value(self.{_an})\n'

                # refr
                elif isinstance(_attr, Refr):
                    if hasattr(_attr.classobj, 'pto_global_key'):
                        _init += f' self.{_an} = kwargs.get("{_an}", None)\n'
                        _todata += f' if self.{_an} is not None:\n'\
                                   f'  data["{_attrdictname}"] = self.{_an}.{_attr.classobj.pto_global_key}\n'
                        _fromdata += f' or_get_value = data.get("{_attrdictname}")\n'\
                                      ' if or_get_value is not None:\n'\
                                     f'  after_loads.append(RefrAfterLoader(self, "{_an}", "{_attr.classobj.pto_classname}", or_get_value))\n'

                    else:
                        raise NoGlobalKey(_attr.classobj)

                # refr (list)
                elif isinstance(_attr, RefrList):
                    if hasattr(_attr.classobj, 'pto_global_key'):
                        _init += f' self.{_an} = kwargs.get("{_an}", [])\n'
                        _todata += f' if self.{_an} is not None:\n' \
                                   f'  data["{_attrdictname}"] = [each.{_attr.classobj.pto_global_key} for each in self.{_an}]\n'
                        _fromdata += f' or_get_value = data.get("{_attrdictname}")\n' \
                                     ' if or_get_value is not None:\n' \
                                     f'  after_loads.append(RefrListAfterLoader(self, "{_an}", "{_attr.classobj.pto_classname}", or_get_value))\n'
                    else:
                        raise NoGlobalKey(_attr.classobj)

                # is a save function
                elif _attr in on_saves:
                    _todata += f' on_saves[{on_saves.index(_attr)}]()\n'

                # is a load function
                elif _attr in on_loads:
                    _todata += f' on_loads[{on_loads.index(_attr)}]()\n'

                # nope!
                else:

                    # is this a correct class?
                    _kind = kind.get(_attr)

                    # is this an attr?
                    if _kind:
                        do = CCFunc()


                        _aft = ''
                        _al = _kind.after_load()
                        if _al:
                            _aft = f' exec({_al})\n'
                        _os = _kind.on_save()
                        if _os:
                            _todata += f' exec({_os})\n'
                        else:
                            _todata += f' data["{_attrdictname}"] = _fix_to(self.{_an})\n'
                        _init += f' self.{_an} = kwargs.get("{_an}", {_kind.default()})\n'
                        _fromdata += f' self.{_an} = data.get("{_attrdictname}", {_kind.default()})\n{_aft}'

        # add global
        if global_key is not None:
            _addglobal = f' if self.{global_key}:\n'\
                         f'  obj_globals["{name}"][self.{global_key}] = self\n'
            _init += _addglobal
            _fromdata += _addglobal

            instances = obj_globals[name]
            setattr(classobj, 'instances', instances)

            def _get(id):
                return instances.get(id)
            setattr(classobj, 'get', _get)

        # execute!
        _glo = globals()
        _body = obj_data_funcs.format(_init or ' pass\n', _todata or ' pass\n', _fromdata or ' pass\n')
        #exec(_body, _glo)
        setattr(classobj, '__init__', classmethod(_init))
        #setattr(classobj, '__init__', _glo['_init'])
        #setattr(classobj, 'pto_to_data', _glo['_todata'])
        #setattr(classobj, 'pto_from_data', _glo['_fromdata'])

        # finish!
        return classobj

    return _wrap



class Obj:
    def __init__(self, default=None):
        self.default = default

# refr
class Refr:

    def __init__(self, classobj):
        self._classobj = classobj

    @property
    def classobj(self):
        if isinstance(self._classobj, str):
            return obj_classes[self._classobj]
        else:
            return self._classobj

class RefrAfterLoader:

    def __init__(self, obj, attr, classname, value):
        self.obj = obj
        self.attr = attr
        self.classname = classname
        self.value = value

    def __call__(self):
        setattr(self.obj, self.attr, obj_globals[self.classname][self.value])

class RefrList:

    def __init__(self, classobj):
        self._classobj = classobj

    @property
    def classobj(self):
        if isinstance(self._classobj, str):
            return obj_classes[self._classobj]
        else:
            return self._classobj

class RefrListAfterLoader:
    def __init__(self, obj, attr, classname, value):
        self.obj = obj
        self.attr = attr
        self.classname = classname
        self.value = value
    def __call__(self):
        setattr(self.obj, self.attr, [obj_globals[self.classname][item] for item in self.value])

# dict refr
dict_refr_none = object()
dict_refrs = []
class DictRefr:

    def __init__(self, source, default=None):
        self.index = len(dict_refrs)
        self.source = source
        self.default = default
        dict_refrs.append(self)

    def value(self, val):
        for key in self.source:
            if self.source[key] == val:
                return key
        return self.default

# other
others = []
class Other:
    def __init__(self, value=None):
        self.value = value
        self.index = len(others)
        others.append(self)

# loading/saving
on_loads = []
on_saves = []

def on_load(func):
    on_loads.append(func)
    return func

def on_save(func):
    on_saves.append(func)
    return func

# special
special_obj = None
class Special:

    def __init__(self, base=object, after_load=None, on_save=None):
        self.base = base

        if after_load is None:
            self.after_load = lambda: ''
        else:
            self.after_load = kind.add_func(after_load, 'self, data')

        if on_save is None:
            self.on_save = lambda: ''
        else:
            self.on_save = kind.add_func(on_save, 'self, data')

    @staticmethod
    def check(obj):
        global special_obj
        if isinstance(obj, Special):
            special_obj = obj
            return True
        else: return False

    @staticmethod
    def default(): return kind.get(special_obj.base).default()

    @staticmethod
    def get_after_load(): return special_obj.after_load()

    @staticmethod
    def get_on_save(): return special_obj.on_save()

# loading and saving
def _numstr(num, size=2):   # number string
    _n = str(num)
    while len(_n) < size:
        _n = '0' + _n
    return _n

def _fix_to(obj):

    # start
    set_kinds()

    # none
    if obj is None:
        return None

    # literals
    elif isinstance(obj, (float, int, bool, str)):
        return obj

    # datetime
    elif isinstance(obj, timetype):
        return f'{obj.year}-{_numstr(obj.month)}-{_numstr(obj.day)} {_numstr(obj.hour)}:{_numstr(obj.minute)}:{_numstr(obj.second)}'

    # timedelta
    elif isinstance(obj, deltatype):
        op = ''
        for full, abrv in [
            ('days', 'days'),
            ('seconds', 'secs'),
            ('microseconds', 'micros'),
        ]:
            val = getattr(obj, full)    # value
            if val:
                if op:
                    op += ' '
                op += f'{val}{abrv}'
        if not op:
            op = '0micros'
        return op

    # list
    elif isinstance(obj, (list, tuple)):
        return [_fix_to(item) for item in obj]

    # dict
    elif isinstance(obj, dict):
        return {_fix_to(key):_fix_to(obj[key]) for key in obj}

    # obj
    elif obj.__class__ in obj_classes.values():
        inst = {'class': obj.pto_classname}
        obj.pto_to_data(inst)
        return inst

def _fix_from(obj, after_loads):

    # start
    set_kinds()

    # none
    if obj is None:
        return None

    # number / bool
    elif isinstance(obj, (float, int, bool)):
        return obj

    # string
    elif isinstance(obj, str):

        # time?
        for pattern in time_formats:
            try:
                tm = to_time_func(obj, pattern)
                return tm
            except:
                pass

        # timedelta?
        try:
            td = to_delta(obj)
            return td
        except NotDelta:
            pass

        # string
        return obj

    # list
    elif isinstance(obj, (list, tuple)):
        return [_fix_from(item, after_loads) for item in obj]

    # dict
    elif isinstance(obj, dict):

        # start
        _obj = {_fix_from(key, after_loads):_fix_from(obj[key], after_loads) for key in obj}

        # is this an object?
        if 'class' in _obj:
            if _obj['class'] in obj_classes:
                inst = obj_classes[_obj['class']]()
                inst.pto_from_data(_obj, after_loads)
                return inst
            else:
                raise NoSuchClass(_obj['class'])

        # dict
        else: return _obj

def _load(dictobj):
    after_loads = []
    op = _fix_from(dictobj, after_loads)
    for al in after_loads:
        al()
    return op

def load (fileobj):
    close = False
    if isinstance(fileobj, str):
        fileobj = open(fileobj, 'r')
        close = True
    else:
        fileobj = fileobj
    op = _load(yaml.load(fileobj, loader))
    if close:
        fileobj.close()
    return op

def loads(filetext):
    return _load(yaml.load(filetext, loader))

def dump(obj, fileobj):
    _cl = False
    if isinstance(fileobj, str):
        _fo = open(fileobj, 'w')
        _cl = True
    else: _fo = fileobj
    _fo.write(dumps(obj))
    if _cl: _fo.close()

def dumps(obj):
    return yaml.dump(_fix_to(obj), Dumper=dumper)

def load_all(dirname):
    out = []
    for loc in os.listdir(dirname):
        out.append(load(dirname + '/' + loc))
    return out
