
# PTObj

### Plain Text Object
A simple system for creating objects with plain-text yamls.

### Basic Use
```python
import ptobj as pt

@pt.new_class()
class Person:
    name = pt.Field()
    age = pt.Field()

billy = Person(name='Billy', age=28)
print(pt.dumps(billy))
```

The output would appear as so
```yaml
age: 28
class: person
name: Billy
```
<br>

### Defining Field Types Using Converter Functions
Here is an example of wanting a datetime to be converted to a pandas timestamp on load
```python
import ptobj as pt
import pandas as pd

@pt.new_class()
class Person:
    name = pt.Field()
    dob = pt.Field(conv=pd.Timestamp)
```
If you want to convert the value when saving use the ```conv_to``` parameter
<br>

### Global Keys and References
When you declare a global key, all instances of that class are stored in a global dictionary for referencing.
You can declare a global key like so:
```python
@pt.new_class(global_key='name')
class Person:
  name=pt.Field()
  age=pt.Field()
```
This allows other objects to save the global key instead of the entire object.
You can use the global key as a reference like so:
```python
@pt.new_class(global_key='name')
class Person:
    name=pt.Field()
    age=pt.Field()
    friend=pt.Field(kind='refr' refr_class='person')
```
<br>

And so this:
```python
billy=Person(name='Billy', age=28)
jackie=Person(name='Jackie', age=34, friend=billy)

print(pt.dumps(jackie))
```

Would output this
```yaml
age: 34
class: person
friend: billy
name: Jackie
```
<br>

Just make sure to have all referenced objects exported too or it won't know where to find the object on load.
You can make a list of references too like so
```python
@pt.new_class(global_key='name')
class Person:
    name=pt.Field()
    age=pt.Field()
    friends=pt.Field(kind='refr_list', refr_class='person')
```

If an object has a global key, you can access all of the instances like this
```python
Person.__pto_instances__
```

You can get an object by its global key like so
```python
pt.get('person', 'billy')
```
<br>

### Using A Strict Single Class
If you want a field to be a strict single class, or a list of single classes, this can allow you to not need the "class" attribute, which can be less tedious in a long list of objects.
You can do so like this
```python
@pt.new_class(global_key='name')
class Person:
    name=pt.Field()
    age=pt.Field()
    best_friend=pt.Field(kind='single_class', refr_class='person')
    friends=pt.Field(kind='single_class_list', refr_class='person')
```
<br>

### Special Attributes
A special field will allow you to set a custom loader and dumper.
```python
@pt.new_class(global_key='name')
class Person:
    
    name = pt.Field()
    age = pt.Field()
    nickname = pt.Field(kind='special')
    
    @nickname.special_to
    def nickname_to(self, data):
        if self.nickname is None:
            data['nickname'] = self.name
        else:
            data['nickname'] = self.nickname
            
    @nickname.special_from
    def nickname_from(self, data):
        if 'nickname' not in data:
            self.nickname = self.name
        else:
            self.nickname = data.get('nickname')
```
<br>

### Post Initiating Functionality
You can add functionality to a class to will happen after the class loads or is initiated
```python
@pt.new_class(global_key='name')
class Person:
    
    name = pt.Field()
    age = pt.Field()
    nickname = pt.Field()

    @pt.after_init
    def person_after_init(self):
        if self.nickname is None:
            self.nickname = self.name

```
